#!/bin/bash

# Logging
# exec > >(tee -i /path/to/sync_log.txt) 2>&1

# Variablen definieren
PRIVATE_REPO_URL="<URL-zum-privaten-Repo>"
PUBLIC_REPO_URL="<URL-zum-öffentlichen-Repo>"
FAKE_USER_NAME="Fiktiver Benutzer"
FAKE_USER_EMAIL="fiktiv@example.com"

# Liste der Branches, die synchronisiert werden sollen
BRANCHES_TO_SYNC=("branch1" "branch2" "branch3") # Ersetze mit den gewünschten Branch-Namen

# Klonen des privaten Repositories, falls noch nicht vorhanden
if [ ! -d "private-repo" ]; then
    git clone $PRIVATE_REPO_URL private-repo
fi

cd private-repo

# Durchlaufen der Branch-Liste
for BRANCH in "${BRANCHES_TO_SYNC[@]}"; do
    # Überprüfen, ob der Branch existiert
    if git show-ref --verify --quiet refs/heads/$BRANCH; then
        # Wechsle zum Branch
        git checkout "$BRANCH"
        # Erstelle einen neuen Branch ohne Historie
        git checkout --orphan "public-$BRANCH"
 
        # Füge alle Dateien hinzu
        git add .
 
        # Commit mit fiktiven Benutzerdaten
        GIT_AUTHOR_NAME="$FAKE_USER_NAME" GIT_AUTHOR_EMAIL="$FAKE_USER_EMAIL" git commit -m "Daily sync from private $BRANCH branch"
 
        # Pushen zum öffentlichen Repository
        git remote add public $PUBLIC_REPO_URL
        git push -f public "public-$BRANCH:$BRANCH"
 
        # Zurückwechseln zum ursprünglichen Branch
        git checkout "$BRANCH"
    else
        echo "Branch $BRANCH existiert nicht, überspringe..."
    fi
done
