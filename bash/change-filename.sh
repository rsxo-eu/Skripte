#!/bin/sh

################################################################################
#                         change-filename.sh                                   #
#                                                                              #
#                                                                              #
# Change History                                                               #
# 06/02/2022  rsxo-eu       Creating first version of this script              #
#                                                                              #
#                                                                              #
################################################################################
################################################################################
################################################################################
#                                                                              #
#  Copyright (C) 2022 rsxo-eu                                                  #
#  https://codeberg.org/rsxo-eu/Skripte                                        #
#                                                                              #
#  Rename file names in a directory.                                           #
#  German umlauts (Ä,Ö,Ü) are rewritten to Ae, Oe, Ue.                         #
#  Certain characters in the file name are removed.                            #
#                                                                              #
#                                                                              #
################################################################################
################################################################################
################################################################################

# Set the variable for bash behavior
shopt -s nullglob
shopt -s dotglob

## Change folder name
folder="/storage/videos"
## ---------------------


sed="/usr/bin/sed"
mv="/usr/bin/mv"
ls="/usr/bin/ls"

### Check if a directory does not exist ###
if [ ! -d "$folder" ]
then
    echo "Directory $folder DOES NOT exists."
    exit 9990 # die with error code 9990
fi

### Check directory is writeable
if [ ! -w "$folder" ]
then
    echo "Path $folder NOT WRITABLE"
    exit 9991 # die with error code 9991
fi

### Check directory is empty
if [ ! "$($ls -A $folder)" ]; then
    echo "$folder is an empty directory"
    exit 9992 # die with error code 9992
fi

cd $folder

# Get all files in folder
for file in ./*
do
  # echo $file
  infile=`echo "${file:2}"|$sed  \
         -e 's|"\"|"\\"|g' \
         -e 's| |\ |g' -e 's|!|\!|g' \
         -e 's|@|\@|g' -e 's|*|\*|g' \
         -e 's|&|\&|g' -e 's|]|\]|g' \
         -e 's|}|\}|g' -e 's|"|\"|g' \
         -e 's|,|\,|g' -e 's|?|\?|g' \
         -e 's|=|\=|g'  `

  outfileNOoe=`echo "${file:2}"| $sed -e 's|ö|oe|g'`
  outfileNOae=`echo $outfileNOoe| $sed -e 's|ä|ae|g'`
  outfileNOue=`echo $outfileNOae| $sed -e 's|ü|ue|g'`
  outfileNOOE=`echo $outfileNOue| $sed -e 's|Ö|OE|g'`
  outfileNOAE=`echo $outfileNOOE| $sed -e 's|Ä|AE|g'`
  outfileNOUE=`echo $outfileNOAE| $sed -e 's|Ü|UE|g'`
  outfileNOss=`echo $outfileNOUE| $sed -e 's|ß|ss|g'`
  outfileNOSPECIALS=`echo $outfileNOss |$sed -e 's|[^A-Za-z0-9._-]|_|g'`
  outfile=${outfileNOSPECIALS}
  if [ "$infile" != "${outfile}" ]
  then
        # Check new filename exists
        if [ -f "$outfile" ]
        then
           outfile="$(date +%Y%m%d-%H%M%S)_$outfile"
        fi

        # Rename file
        # echo "filename changed for " $infile " in " $outfile
        $mv "$infile" ${outfile}
  fi
done

exit
